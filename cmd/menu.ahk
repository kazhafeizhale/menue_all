﻿#include <log4ahk>
#include <py>
SetBatchLines -1
py.allspell_muti("hetong")
OnMessage(0x100, "GuiKeyDown")
OnMessage(0x6, "GuiActivate")
#Persistent
#SingleInstance force
;Run,*RunAs %A_ScriptDir%\menueSearch.ahk
#include %A_ScriptDir%\cmd\menufunc.ahk
#include %A_ScriptDir%\cmd\menucmd.ahk
begin := 1
!q::
Gui +LastFoundExist
if WinActive()
{
    goto GuiEscape
}
Gui Destroy
Gui, Color,,0x000000
Gui Font, s11
Gui Margin, 0, 0
Gui, Font, s10 cLime, Consolas
Gui Add, Edit, x20 w500 vQuery gType
Gui Add, Text, x5 y+2 w15 +Background0x000000, 1`n2`n3`n4`n5`n6`n7`n8`n9 
Gui Add, ListBox, x+0 yp-2 w500 r21 vCommand gSelect AltSubmit +Background0x000000
Gui Add, StatusBar, +Background0x000000
Menu, MyMenuBar, Add, &File, :Menu
Gui, Menu, MyMenuBar,
Gui, +HwndMyGuiHwnd
Gui +ToolWindow +Resize +MinSize +MinSize200x +MaxSize +MaxSize%A_ScreenWidth%x
Gui, -Caption +AlwaysOnTop +ToolWindow -SysMenu

if !(cmds := MenuGetAll(MyGuiHwnd))
{
    Send {Alt}
    return
}
;log.info(cmds)
gosub Type
WinGetTitle title, ahk_id %MyGuiHwnd%
title := RegExReplace(title, ".* - ")
Gui Show,, Searching menus of:  %title%
GuiControl Focus, Query
return

MenuHandler:
msgbox,ok
return

Type:
SetTimer Refresh, -10
return

Refresh:
GuiControlGet Query
r := cmds
if (Query != "")
{
    StringSplit q, Query, %A_Space%
    Loop % q0
        r := Filter_new(r, q%A_Index%, c)
}
rows := ""
row_id := []
Loop Parse, r, `n
{
    RegExMatch(A_LoopField, "(\d+)`t(.*)", m)
    row_id[A_Index] := m1
    rows .= "|"  m2
}
GuiControl,, Command, % rows ? rows : "|"
if (Query = "")
    c := row_id.MaxIndex()

Select:

GuiControlGet Command
if !Command
    Command := 1
Command := row_id[Command]
SB_SetText("Total " c " results`t`tID: " Command)
if (A_GuiEvent != "DoubleClick")
{
    return
}

Confirm:
if !GetKeyState("Shift")
{
    gosub GuiEscape
}
DllCall("SendNotifyMessage", "ptr", MyGuiHwnd, "uint", 0x111, "ptr", Command, "ptr", 0)
return

GuiEscape:
Gui,Hide
cmds := r := ""
return

GuiSize:
GuiControl Move, Query, % "w" A_GuiWidth-20
GuiControl Move, Command, % "w" A_GuiWidth-20
return

GuiActivate(wParam)
{
    if (A_Gui && wParam = 0)
        SetTimer GuiEscape, -5
}

GuiKeyDown(wParam, lParam)
{
    if !A_Gui
        return
    if (wParam = GetKeyVK("Enter"))
    {
        gosub Confirm
        return 0
    }
    if (wParam = GetKeyVK(key := "Down")
     || wParam = GetKeyVK(key := "Up"))
    {
        GuiControlGet focus, FocusV
        if (focus != "Command")
        {
            GuiControl Focus, Command
            if (key = "Up")
                Send {End}
            else
                Send {Home}
            return 0
        }
        return
    }
    if (wParam >= 49 && wParam <= 57 && !GetKeyState("Shift") && GetKeyState("LCtrl"))
    {
        SendMessage 0x18E,,, ListBox1
        GuiControl Choose, Command, % wParam-48 + ErrorLevel
        GuiControl Focus, Command
        gosub Select
        return 0
    }
    if (wParam = GetKeyVK(key := "PgUp")
     || wParam = GetKeyVK(key := "PgDn"))
    {
        GuiControl Focus, Command
        Send {%key%}
        return
    }
}

keyValueFind(haystack,needle)
{
    ;拼音首字母转换
    ;msgbox,% haystack
    haystack .= py.allspell_muti(haystack) py.initials_muti(haystack) 
    ;log.info(haystack)
	findSign:=1
	needleArray := StrSplit(needle, " ")
	Loop,% needleArray.MaxIndex()
	{
		if(!InStr(haystack, needleArray[A_Index], false))
		{
			findSign:=0
			break
		}	
	}
	return findSign
}
Filter_new(s, q, ByRef count)
{
    ;建立数组 [{“a" : "原始值", "b" : "首字母"}]
    ;匹配
    s := StrSplit(s, ["`r","`n"])
    result := ""
    result := ""
    VarSetCapacity(result,0)
    VarSetCapacity(result,4000)
    count := 0
    for k,v in s
    {
        if(keyValueFind(v, q))
        {
            result .= v "`n"
            count += 1
        }
    }
    return SubStr(result, 1, -1)
}

Filter(s, q, ByRef count)
{
    ;转换_拼音首字母,或者 建立数组 [{“a" : "", "b" :""}]
    ;匹配，删除
    if (q = "")
    {
        StringReplace s, s, `n, `n, UseErrorLevel
        count := ErrorLevel
        return s
    }
    i := 1
    match := ""
    result := ""
    count := 0
    while i := RegExMatch(s, "`ami)^.*\Q" q "\E.*$", match, i + StrLen(match))
    {
        result .= match "`n"
        count += 1
    }
    return SubStr(result, 1, -1)
}

MenuGetAll(hwnd)
{
    if !menu := DllCall("GetMenu", "ptr", hwnd, "ptr")
        return ""    
    MenuGetAll_sub(menu, "", cmds)
    return cmds
}

MenuGetAll_sub(menu, prefix, ByRef cmds)
{
    Loop % DllCall("GetMenuItemCount", "ptr", menu)
    {
        VarSetCapacity(itemString, 2000)
        if !DllCall("GetMenuString", "ptr", menu, "int", A_Index-1, "str", itemString, "int", 1000, "uint", 0x400)
            continue
        StringReplace itemString, itemString, &
        itemID := DllCall("GetMenuItemID", "ptr", menu, "int", A_Index-1)
        if (itemID = -1)
        if subMenu := DllCall("GetSubMenu", "ptr", menu, "int", A_Index-1, "ptr")
        {
            MenuGetAll_sub(subMenu, prefix itemString " > ", cmds)
            continue
        }
        cmds .= itemID "`t" prefix RegExReplace(itemString, "`t.*") "`n"
    }
}