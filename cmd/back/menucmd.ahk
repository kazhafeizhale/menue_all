﻿!c::
Menu,Menu,Show
return
Function(Item,Index,Menu)
{
    log.info(item, index, menu)
    log.info(A_ThisMenuItem  " "A_ThisMenu  " " A_ThisMenuItemPos)
	fn := Func(Item)
	fn.Call()
 }

menueTool__75RWE8UGE()
{
	CommandText =
    (%
CommandText =
`(%
	
`)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog`(Instruction, Content, "", 0x1, 0`)
    )
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog`(Instruction, Content, "", 0x1, 0`)	
}
双重引用_75RWBBGYA()
{
	CommandText =
    (%
% %d%  表达式里面的% % 表示引用对象为变量名字
例如：
	cc:=10
	a:="c"
	msgbox,% %a%c  ;表示变量cc的值 
    )    
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

inputoutput_75RWBBGYL()
{
	CommandText =
    (%
变量的名字：例如 va1:=10  传入 va1
也可以是动态变量（变量的引用） 例如 va2 = "va1"  传入 %va2%
    )    
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

数字参数_75RWBBGYV()
{
	CommandText =
    (
表达式
    )
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

不存在某个单词的行_75RW7QWBP()
{
	CommandText =
    (
^((?!printf).)*$
	)
	clipboard:=CommandText
	ToolTip("clipboard")	
}

并行处理_75RW5XX6Q()
{
	CommandText =
    (
fork
:Treatment 1;
fork again
:Treatment 2;
end fork
	)
	clipboard:=CommandText
	ToolTip("clipboard")

}
注释_75RW5XXCP()
{
	CommandText =
    (
floating note left: This is a note
note right
  This note is on several
end note
	)
	clipboard:=CommandText
	ToolTip("clipboard")
}
连接器_75RW5XXN4()
{
	CommandText =
    (
:Some activity;
(A)
detach
(A)
:Other activity;
	)
	clipboard:=CommandText
	ToolTip("clipboard")
}
开始结束_75RW5XVF9()
{
	CommandText =
	(%
@startuml
start

stop
@enduml
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
do循环语句_75RW5XVIA()
{
	ToolTip("clipboard")	
	CommandText =
	(%
repeat
:read;
repeat while ()
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

while循环语句_75RW5XWSY()
{
	CommandText =
    (
while (data)
  :read;
endwhile
		
	)
	clipboard:=CommandText
	Instruction := CommandText
	ToolTip("clipboard")	
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}


条件语句_75RW5XVHE()
{
	CommandText =
    (
start
if (condition A) then (yes)
  :Text 1;
elseif (condition B) then (yes)
  :Text 2;
  stop
elseif (condition C) then (yes)
  :Text 3;
elseif (condition D) then (yes)
  :Text 4;
else (nothing)
  :Text else;
endif
stop
	)
	clipboard:=CommandText
	Instruction := CommandText
	ToolTip("clipboard")
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}


instance例子_75NKXRSID()
{
	inputvar = 
    (
template<typename AnyType>
void sWap(AnyType &a,AnyType &b)
{
    AnyType temp;
    temp = a;
    a = b;
    b = temp;
}
    )
	clipboard:=inputvar
	ToolTip("Clipboard")  
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

vim_75RXROP91()
{
	CommandText =
    (%
重要的概念
	增：
		a/A(append)   i/i(insert)  o/O(open a line)
	删：
		diw(delete inner word)   daw(delete around word)  dd(delete a line word)
	改：
		c (change as word)  ci字符(例如：ci" 删除引号里面的内容，并改成插入模式)   ct字符(删除到字符的内容，并改成插入模式)
	查：
		f字符(向前查找字符)   F字符(向后查找字符)  ;(重复上一个命名)
		命令模式下：   /字符(高亮字符，并向前查)   ?字符(高亮字符，并向后查)
	
	光标移动：
		h j k l (左 下 上  右)
		w(前字符)   b()
		:数字   数字G    gg   G    0(行首)  $(行尾) 
		
		ctrl+o(向后跳转)   ctrl+i(向前跳转)
		
	翻页：
		Ctrl+f(forward  page)   ctrl+b(back page)
		ctrl+e(屏幕向下滚动一行)  ctrl+y(屏幕向上滚动一行)
		ctrl+d(down)   ctrl+u()
	
	编辑：
		u(撤销上一步的操作)    Ctrl+r(恢复上一步被撤销的操作)
	设置:
		set clipboard=unnamed
	
    )    
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
tooltip_75NO1OONN()
{
	CommandText =
    (
ToolTip(label){
	ToolTip, %label%, 930, 650
	
	SetTimer, RemoveToolTip, 1000
	return
	RemoveToolTip:
	SetTimer, RemoveToolTip, Off
	ToolTip
	Return
}
	)
	
	clipboard:=CommandText
	ToolTip("clipboard")
}


Expression_75NKYYSWS()
{
	CommandText =
    (
重要的概念：
	. :表示前面任意字符
	+ :表示前面一个或者多个任意字符
	？:表示前面零个或者一个任意字符
	.*  :表示任意多个字符
	.+  :表示一个或者多个任意字符
	.?  :表示零个或者一个任意字符
	
	断言：
	正向断言：(?=...) (?!...)
	反向断言：(?<=...) (?<!...)
    )    
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
concept概念_75NKXRQ59()
{
	CommandText =
    (
重载的概念：
    一、通过函数名字和参数列表来区分（参数的个数，参数的类型），不通过返回值判断
    二、如果没有重载参数会强制化转换，但是如果有多个转化的可能，就会报错。
        unsigned int value = 10;
        func(int );func(double );报错，编译器没法判断。
    三、非const可以传给const，const不可以传给非const。
    
    )
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
修改密码_75NKXPWLQ()
{
	UserInput:=""    
	Var =
        (
-a 全部
        )        
	InputBox, UserInput, Phone Number,% Var,
	inputvar = 
        (
passwd            
        )        
	inputvar:=inputvar . UserInput
	ascinput(inputvar)    
	
}
杀死进程_75NKXPWMB()
{    
	Var =
        (
        )
	inputvar = 
        (
kill            
        )   
	inputvar:= inputvar . UserInput
	ascinput(inputvar)
}
host命令_75NKXPWMG()
{
	Var =
        (
-a：显示详细的DNS信息；
-c<类型>：指定查询类型，默认值为“IN“；
-C：查询指定主机的完整的SOA记录；
-r：在查询域名时，不使用递归的查询方式；
-t<类型>：指定查询的域名信息类型；
-v：显示指令执行的详细信息；
-w：如果域名服务器没有给出应答信息，则总是等待，直到域名服务器给出应答；
-W<时间>：指定域名查询的最长时间，如果在指定时间内域名服务器没有给出应答信息，则退出指令；
-4：使用IPv4；
-6：使用IPv6.
        )
	InputBox, UserInput, Phone Number,% Var,
	
	inputvar = 
        (
host            
        )
	inputvar:= inputvar . " " . UserInput
	ascinput(inputvar)    
	
}

转化为c代码_75NKXPVOD()
{
	Var =
    (
    )
	inputvar = 
    (
CForm[expr]
    )
	clipboard:=inputvar . var
	ToolTip("Clipboard")   
	
	
}
窗口置顶_75NKXPVXG()
{
	Var =
    (
    )
	inputvar = 
    (
MouseGetPos, MouseX, MouseY, MouseWin
; 似乎有必要首先关闭任何现有的透明度:
WinSet, AlwaysOnTop, On, ahk_id %MouseWin%
    )
	clipboard:=inputvar . var
	ToolTip("Clipboard")
	
}
引用的使用_75NKXPVS5()
{        
	CommandText = 
    (
const使用方法
    一、修改传入函数的值时
        1、默认数据类型用指针。int ,float, double ,char
        2、结构体用指针或者引用
        3、数组只能用指针
        4、类对象只能用引用
    二、不修改传入函数的值时
        1、默认数据类型用传值
        2、结构体用const 指针或者const引用
        3、数组只能用const 指针
        4、类对象用const引用
    )    
	Instruction := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
;说明 在QQ聊天窗口中是乱码，需要发送UTF-16BE编码
ascinput(string){
	u :=  A_IsUnicode ? 2 : 1 ;Unicode版ahk字符长度是2
	length:=StrPut(string,"CP0")
	if(A_IsUnicode)
	{
		VarSetCapacity(address,length),StrPut(string,&address,"CP0")
	}
	else
		address:=string
	VarSetCapacity(out,2*length*u)
	index =0
	Loop
	{
		index += 1
		if (index>length-1)
			Break
		asc := NumGet(address,index-1,"UChar")
		if asc > 126
		{
			index += 1
			asc2 := NumGet(address,index-1,"UChar")
			asc := asc*256+asc2
		}
		sendinput, % "{ASC " asc "}"
	}
}
ascaltinput(string){
	u :=  A_IsUnicode ? 2 : 1 ;Unicode版ahk字符长度是2
	length:=StrPut(string,"CP0")
	if(A_IsUnicode)
	{
		VarSetCapacity(address,length),StrPut(string,&address,"CP0")
	}
	else
		address:=string
	VarSetCapacity(out,2*length*u)
	index =0
	Loop
	{
		index += 1
		if (index>length-1)
			Break
		asc := NumGet(address,index-1,"UChar")
		if asc > 126
		{
			index += 1
			asc2 := NumGet(address,index-1,"UChar")
			asc := asc*256+asc2
		}
		StringSplit, var, asc,
		Loop % var0
		{
			str .= "{Numpad" var%A_index% "}"
		}
		sendinput, {AltDown}%str%{Altup}
		str =
	}
}
ToolTip(label){
	ToolTip, %label%, 930, 650
	
	SetTimer, RemoveToolTip, 1000
	return
	RemoveToolTip:
	SetTimer, RemoveToolTip, Off
	ToolTip
	Return
}
TaskDialog(Instruction, Content := "", Title := "", Buttons := 1, IconID := 0, IconRes := "", Owner := 0x10010) {
	Local hModule, LoadLib, Ret
	
	If (IconRes != "") {
		hModule := DllCall("GetModuleHandle", "Str", IconRes, "Ptr")
		LoadLib := !hModule
            && hModule := DllCall("LoadLibraryEx", "Str", IconRes, "UInt", 0, "UInt", 0x2, "Ptr")
	} Else {
		hModule := 0
		LoadLib := False
	}
	
	DllCall("TaskDialog"
        , "Ptr" , Owner        ; hWndParent
        , "Ptr" , hModule      ; hInstance
        , "Ptr" , &Title       ; pszWindowTitle
        , "Ptr" , &Instruction ; pszMainInstruction
        , "Ptr" , &Content     ; pszContent
        , "Int" , Buttons      ; dwCommonButtons
        , "Ptr" , IconID       ; pszIcon
        , "Int*", Ret := 0)    ; *pnButton
	
	If (LoadLib) {
		DllCall("FreeLibrary", "Ptr", hModule)
	}
	
	Return {1: "OK", 2: "Cancel", 4: "Retry", 6: "Yes", 7: "No", 8: "Close"}[Ret]
}

fprintf_75RWE8Q55()
{
	CommandText =
(%
int printf(const char *format, ...)
)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

fgets_75RWE8PRA()
{
	CommandText =
	(%
char *fgets(char *buffer, int buffer_size, FILE *stream);
如果在任何字符读取前就到达了文件尾，缓冲区就未进行修改，fgets函数返回一个NULL指针。否则，fgets返回它的第1个参数（指向缓冲区的指针）。这个返回值通常只用于检查是否到达了文件尾。
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
清除所有定义_75RWEA0FM()
{
	CommandText =
	(%
Clear["Global``*"]
Clear["System``*"]
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	;TaskDialog(Instruction, Content, "", 0x1, 0)

}

Tabel__75RWEA6JH()
{
CommandText =
(%
	Table[i^2, {i, 6}]
	{i,a,b,c}  变量i,从 a 到 b 间隔 c, 产生列表
	Table[f,{i,n}]	                                             通过计算 i=1,2,\[Ellipsis],n 时 f 的值，构造一个 n 维向量
	Array[a,n]	                                                构造形如 {a[1],a[2],\[Ellipsis]} 的 n 维向量
	Range[n]	                                                 建立列表 {1,2,3,\[Ellipsis],n}
	Range[Subscript[n, 1],Subscript[n, 2]]	                    建立列表 {Subscript[n, 1],Subscript[n, 1]+1,\[Ellipsis],Subscript[n, 2]}
	Range[Subscript[n, 1],Subscript[n, 2],dn]	                  建立列表 {Subscript[n, 1],Subscript[n, 1]+dn,\[Ellipsis],Subscript[n, 2]}
	list[[i]] \[ThinSpace]or \[ThinSpace] Part[list,i]             	取出向量 list 的第 i 个元素
	Length[list]	                                                给出 list 的元素个数
	c v	                                                             数乘向量
	a.b	                                                              向量的点积
	Cross[a,b]	                                                    向量的叉积 （也输入为 a*b）
	Norm[v]	                                                       向量的欧式范数（norm）


)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

打开_75RWFHH1F()
{
CommandText =
(%
void open(const char* szFileName, int mode)
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)

}
动态内存分配_75RWIGF3C()
{
CommandText =
(%
	type * p = new type[n];
	delet p;
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

查找_75RXS8UAR()
{
	CommandText =
	(%
ctr+shift+f
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	;TaskDialog(Instruction, Content, "", 0x1, 0)
	sendinput,^+f
}

替换_75RXS8UAV()
{
	sendinput,^+h
}

转到定义_75RXS8UAZ()
{
	sendinput,{f12}
}

查看所有引用_75RXS8UB5()
{
	sendinput,+{f12}
}
切换文件_75RXS8UBE()
{
	sendinput,^+t
}

查看类图_75RXS8UBL()
{
	sendinput,^+c
}
introduce_75RWE8PI4(){
}
fgetc__75RWE8PIW(){
}
getchar_75RWE8PP8(){
}
getc_75RWE8PQJ(){
}
fputc_75RWE8PQQ(){
}
putc_75RWE8PQY(){
}
putchar_75RWE8PR5(){
}
gets_75RWE8PRH(){
}
fputs_75RWE8PRM(){
}
puts_75RWE8PRT(){
}
fscanf_75RWE8Q2Z(){
}
scanf_75RWE8Q4H(){
}
sscanf_75RWE8Q4X(){
}
printf_75RWE8Q5G(){
}
sprintf_75RWE8Q5T(){
}
Tabel_75RWEA6JH(){
}
text_75RWBBGYQ(){
}
获取项_75RWIG7ZI(){
}
设置赋值项_75RWIG804(){
}
插入_75RWIG80F(){
}
追加_75RWIG80S(){
}
移除_75RWIG828(){
}
获取_75RWIG82Y(){
}
赋值设置_75RWIG84X(){
}
移除删除_75RWIG859(){
}
枚举项_75RWIG85T(){
}
关闭_75RWFHH1N(){
}
读取一行_75RWFHH3D(){
}
断言前面有_75RW7QZT4(){
}
断言前面没有_75RW7QZXZ(){
}
class类_75RXS9XVK(){
}
const_75RXSW2CY(){
}


开始结束_75RZE1N8V(){
	CommandText =
	(%
@startmindmap

@endmindmap
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
}
标题_75RZE1N9A(){

}

读取文件到string_75S1832QO(){

	CommandText =
	(%
fstream myFile;
myFile.open("ahker.txt");
istreambuf_iterator<char> begin(myFile), end;
string fileDate(begin, end);
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	;TaskDialog(Instruction, Content, "", 0x1, 0)
}

分割字符串到vector_75S1833AD(){
	CommandText =
	(%
vector<string> deString;
const char *delim = "\n";
const char *p = strtok((char *)fileDate.c_str(), delim);
while (p)
{
	deString.push_back(p);
	p = strtok(NULL, delim);
}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	;TaskDialog(Instruction, Content, "", 0x1, 0)
}


大小写转换_75S2PO8LG(){
	CommandText =
	(%
transform(s.begin(),s.end(),s.begin(),::tolower);
/*
for( int i = 0; i < s.size(); i++ )
{
	s[i] = tolower(s[i]);
}
*/
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

json作为配置文件_75S2POULO(){
CommandText =
(%
#include C:\Users\ahker\Desktop\我的AHK程序\库\v1\json.ahk

json_path := A_ScriptDir . "/json.json"
config := {}
loadconfig(config)
config.parameter1 := "ahk2"
saveconfig(config)
loadconfig(ByRef config)
{
    Global json_path
    FileRead, OutputVar,% json_path
    config := json_toobj(outputvar)    
}

saveconfig(config)
{
    global json_path
    str := json_fromobj(config)
    FileDelete, % json_path
    FileAppend,% str,% json_path
}
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)	
}

cjson解析jsondll调用_75S2POUYY(){
CommandText =
(%
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include "cJSON.h"
extern "C" _declspec(dllexport) int dlltest(char arr[200],char chararr[200])
{
	FILE *fp = NULL;
	fp = fopen("dlldate.txt","a+");
	cJSON *json;
	cJSON *item = NULL;
	json=cJSON_Parse(arr);

	if (!json) {fprintf(fp,"Error before: [%s]\n",cJSON_GetErrorPtr());}
	else
	{
		item = cJSON_GetObjectItem(json, "parameter1");
		fprintf(fp,"%s\n", item->valuestring);
		item = cJSON_GetObjectItem(json, "parameter2");
		fprintf(fp,"%s\n",item->valuestring);
		item = cJSON_GetObjectItem(json, "parameter3");
		//fprintf(fp,"type is %d\n",item->type);
		fprintf(fp, "%g\n", item->valuedouble);
		cJSON *item1 = NULL;
		item1 = cJSON_GetObjectItem(json, "parameter4");
		item = cJSON_GetObjectItem(item1, "wed1");
		if (cJSON_IsInvalid(item))
		{
			fprintf(fp,"type is %d\n",item1->type);
			fprintf(fp, "%s\n", item->valuestring);
		}
		std::string parameter1 = cJSON_GetObjectItem(json, "parameter1")->valuestring;
		if (parameter1 == "ahk2")
		{
			char *m_string = strdup("heqiang");
			item = cJSON_GetObjectItem(json, "parameter2");
			if (item->valuestring != NULL)
			{
				free(item->valuestring);
				item->valuestring = m_string;
			}
			else
			{
				cJSON_AddItemToObject(json, "parameter2", cJSON_CreateString("heqiang"));
			}
			fprintf(fp, "add\n");
		}
		char *out = NULL;
		out = cJSON_Print(json);
		strcpy(chararr, out);
		fprintf(fp, "%s", out);
		free(out);
		cJSON_Delete(json);
	}
	if (NULL == fp)
	{
		fclose(fp);
	}
	return 10;
}
}
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

dll调用json_75S2TTUE2(){
	CommandText =
	(%
#Include C:\Users\ahker\Desktop\我的AHK程序\库\json相关\json.ahk
json_path := A_ScriptDir . "/json.json"
FileRead, OutputVar,% json_path

config := json_toobj(outputvar)
mystr := json_fromobj(config)
MsgBox,% mystr
hModule := DllCall("LoadLibrary", "Str", "C:\Users\ahker\source\repos\AHKDLL\x64\Release\AHKDLL.dll", "Ptr")  

inputstr:="" ;输入字符变量
VarSetCapacity(inputStr,400,0)
StrPut(mystr, &inputStr,"cp0")

outstr := ""
VarSetCapacity(outstr,400,0)
result := DllCall("AHKDLL\dlltest","Str",inputStr,"Str",outstr,"Cdecl ")

if(ErrorLevel != 0)
{
    MsgBox,% "Errorlevel"   ErrorLevel
    return
}
MsgBox,% "outstr Return is" StrGet(&outstr,400,"CP0")
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

sort_75S2TU2NA(){
CommandText =
(%
bool compare(int a, int b)
{
	return a < b;
}
std::sort(myHeights.begin(), myHeights.end(), compare);
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

终端库console_75S2TVJ9M(){
	CommandText =
	(%
#include C:\Users\ahker\Desktop\我的AHK程序\库\终端库\Class_Console-master\Class_Console.ahk
Class_Console("a",5,5,800,800)
a.show()
a.log("hello world")
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

并行处理_75S2W9ZV7(){
	CommandText =
	(%
start

if (multiprocessor?) then (yes)
  fork
    :Treatment 1;
  fork again
    :Treatment 2;
  end fork
else (monoproc)
  :Treatment 1;
  :Treatment 2;
endif
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
注释_75S2W9ZVH(){
	CommandText =
	(%
floating note left: This is a note
:foo2;
note right
  This note is on several
  //lines// and can
  contain <b>HTML</b>
  ====
  * Calling the method ""foo()"" is prohibited
end note
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
连接器_75S2W9ZVU(){
	CommandText =
	(%
:Some activity;
(A)
detach
(A)
:Other activity;
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
组合分类_75S2W9ZXC(){
	CommandText =
	(%
partition Initialization {
    :read config file;
    :init internal variable;
}
partition Running {
    :wait for user interaction;
    :print information;
}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
泳道_75S2W9ZXN(){
	CommandText =
	(%
|Swimlane1|
start
:foo1;
|#AntiqueWhite|Swimlane2|
:foo2;
:foo3;
|Swimlane1|
:foo4;
|Swimlane2|
:foo5;
stop
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
分离_75S2W9ZXX(){
	CommandText =
	(%
 :start;
 fork
   :foo1;
   :foo2;
 fork again
   :foo3;
   detach
 endfork
 if (foo4) then
   :foo5;
   detach
 endif
 :foo6;
 detach
 :foo7;
 stop
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

tex格式导入__75S2Y1NCV(){
	CommandText =
(%
ToExpression["", TeXForm]
)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

tex格式导出__75S2Y1NPH(){
	CommandText =
	(%
TeXForm[%]
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

cjsonobj解析jsondll调用_75S4BEOOZ(){
	CommandText =
	(%
using namespace std;
#include "CJsonObject.hpp"
extern "C" _declspec(dllexport) int dlltest(char arr[2000], char chararr[2000])
{
	int rtn = OK;
	double iValue;
	double fTimeout;
	std::string strValue;

	neb::CJsonObject obj(arr);

	if (obj.Get("city", strValue))
	{
		cout << "city:  " << strValue << endl;
	}
	else
	{
		cout << "no city element" << endl;
	}

	neb::CJsonObject rtnObj;
	rtnObj.Add("name", "ahker");
	strcpy(chararr, rtnObj.ToString().c_str());
	return rtn;
}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

dll_json_75S4BGEBZ(){
	CommandText =
	(%
'''
by:ahker 2020年8月1日 18:26:37
using: python json dll
'''

from ctypes import *
import json

dll = windll.LoadLibrary('C:/Users/ahker/source/repos/AHKDLL/x64/Release/AHKDLL.dll')

date = {"name":"ahker"}

str = json.dumps(date) 
p=c_wchar_p(str)

pStr = create_string_buffer(2000, '\0')
rst=dll.dlltest(p, pStr)

text = json.loads(pStr.value)
print(text['name'])
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

cmake__75S4DV03G(){

	CommandText =
	(%
cmake_minimum_required(VERSION 2.8)
project(demo)

add_executable(main main.cpp cJSON.c)
target_link_libraries (main libncurses.so)
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

读取文件内容到变量_75S4KET1L(){
	CommandText =
	(%
		void doit(char *text)
		{
			char *out;cJSON *json;
			
			json=cJSON_Parse(text);
			if (!json) {printf("Error before: [%s]\n",cJSON_GetErrorPtr());}
			else
			{
				out=cJSON_Print(json);
				cJSON_Delete(json);
				printf("%s\n",out);
				free(out);
			}
		}

		void dofile(char *filename)
		{
			FILE *f;long len;char *data;
			
			f=fopen(filename,"rb");fseek(f,0,SEEK_END);len=ftell(f);fseek(f,0,SEEK_SET);
			data=(char*)malloc(len+1);fread(data,1,len,f);fclose(f);
			doit(data);
			free(data);
		}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
cjson解析_75S4KVM5D(){
	CommandText =
	(%
		int dlltest(void)
		{
			int rtn = OK;
			FILE *fp;
			fp = fopen("config.json" , "r");
			fseek( fp , 0 , SEEK_END );
			int file_size;
			file_size = ftell( fp );
			char *tmp;
			fseek( fp , 0 , SEEK_SET);
			tmp =  (char *)malloc( file_size * sizeof( char ) );
			fread( tmp , file_size , sizeof(char) , fp);//个人觉得第三个参数不对
			printf("%s end\n" , tmp );
			if (NULL == fp)
			{
				fclose(fp);
			}

			cJSON *json = NULL;
			cJSON *json1 = NULL;
			cJSON *json2 = NULL;
			cJSON *json3 = NULL;
			cJSON *item = NULL;
			json=cJSON_Parse(tmp);
			string valueString;
			double valueDouble;
			typedef struct
			{
				char brother[20];
				double array[4];
			}OBJ_STRUCT;
			typedef struct
			{
				char city[20];
				char name[20];
				double age;
				OBJ_STRUCT obj;
			}PEPLE_STRUCT;

			typedef struct
			{
				char parameter1[20];
				PEPLE_STRUCT parameter2;
			}JSON_STRUCT;
			JSON_STRUCT jsonDate;

			if (!json && rtn == OK) {printf("Error before: [%s]\n",cJSON_GetErrorPtr());}
			else
			{
				if ((item = cJSON_GetObjectItem(json, "parameter1"))->type == cJSON_String && rtn == OK)
				{
					strcpy(jsonDate.parameter1, item->valuestring);
				}else{ rtn = 0x1;}
				if ((json1 = cJSON_GetObjectItem(json, "parameter2"))->type == cJSON_Object && rtn == OK)
				{
					if ((item = cJSON_GetObjectItem(json1, "age"))->type == cJSON_Number && rtn == OK)
					{
						jsonDate.parameter2.age = item->valuedouble;
					}else { rtn = 0x1; }
					if ((item = cJSON_GetObjectItem(json1, "city"))->type == cJSON_String && rtn == OK)
					{
						strcpy(jsonDate.parameter2.city, item->valuestring);
					}else { rtn = 0x1; }
					if ((json2 = cJSON_GetObjectItem(json1, "obj"))->type == cJSON_Object && rtn == OK)
					{
						if ((item = cJSON_GetObjectItem(json2, "brother"))->type == cJSON_String && rtn == OK)
						{
							strcpy(jsonDate.parameter2.obj.brother, item->valuestring);
						}else { rtn = 0x1; }
						if ((json3 = cJSON_GetObjectItem(json2, "array"))->type == cJSON_Array && rtn == OK)
						{
							for(int i = 0; i < cJSON_GetArraySize(json3); i++)
							{
								jsonDate.parameter2.obj.array[i] = cJSON_GetArrayItem(json3, i)->valuedouble;
							}
						}else { rtn = 0x1; }
					}else { rtn = 0x1; }
				}else{ rtn = 0x1;}
			}
			if (rtn == OK)
			{
				std::cout << jsonDate.parameter2.city << endl;
				std::cout << "age:" << jsonDate.parameter2.age << endl;
				std::cout << "brother:" << jsonDate.parameter2.obj.brother << endl;
				for (int i = 0; i < 4; i++)
				{
					std::cout << "arrar" << i << ";" << jsonDate.parameter2.obj.array[i] << endl;
				}
				if (strcmp(jsonDate.parameter2.city, "shanghai"))
				{
					std::cout << "ok" << endl;
				}
			}
			cJSON_Delete(json);
			if (tmp != NULL)
			{
				free(tmp);
			}
			return 10;
		}
		{
			"parameter1":"贺通10",
			"parameter2":{
				"city":"shanghai上海",
				"name":"ahker",
				"age":23,
			"obj": {
			"brother": "heliang",
			"array": [1,2,34,5]
			}
			}
		}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
plot_chartctrl_75S4L0CVQ(){
	CommandText =
	(%
		//坐标轴
		CChartAxis *pAxis= NULL; 
		pAxis = m_ChartCtrl1.CreateStandardAxis(CChartCtrl::BottomAxis);
		pAxis->SetAutomatic(true);
		pAxis = m_ChartCtrl1.CreateStandardAxis(CChartCtrl::LeftAxis);
		pAxis->SetAutomatic(true);

		//设置标题
		TChartString str1;
		str1 = _T("IDC_ChartCtrl1 - m_ChartCtrl1");
		m_ChartCtrl1.GetTitle()->AddString(str1);
		m_ChartCtrl1.GetLeftAxis()->GetLabel()->SetText(_T("纵坐标"));

		void chart::DataShow(double *xb, double *yb, int len) {
			m_ChartCtrl.EnableRefresh(false);
			CChartLineSerie *pLineSerie;
			m_ChartCtrl.RemoveAllSeries();
			pLineSerie = m_ChartCtrl.CreateLineSerie();
			pLineSerie->SetSeriesOrdering(poNoOrdering);//设置为无序
			pLineSerie->AddPoints(xb, yb, len);
			UpdateWindow();
			m_ChartCtrl.EnableRefresh(true);
		}

	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

自定义消息_75S4NZKQZ(){

CommandText =
(%
#define WM_MyMessage (WM_USER+100)

afx_msg LRESULT OnMyMessage(WPARAM wParam, LPARAM lParam); // add lyw
DECLARE_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
   ……
  ON_MESSAGE(WM_MyMessage, OnMyMessage)
END_MESSAGE_MAP()

LRESULT CTestDlg::OnMyMessage(WPARAM wParam, LPARAM lParam)
{
  //MessageBox("recv msg success");
  //添加自己的消息处理
  ……
  return 0;  
}

SendMessage( WM_MyMessage, 0, 0);
或者
PostMessage(WM_MyMessage, 0, 0); 

::PostMessage(GetSafeHwnd(), WM_USER_THREADEND, 0, 0);

https://www.cnblogs.com/rainbow70626/p/8034910.html

)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

cform类创建_75S4PMYR5(){
CommandText =
(%
第一步：新建diolog对话框，关联类（类型选择diog）
第二步：修改cpp和h文件
.cpp

// CgEditControl.cpp : 实现文件
#include "stdafx.h"

#include "cg2019HkrFillPoly.h"//注意修改！！！！！
#include "CCgEditControl.h"//注意修改！！！！

// CCgEditControl

IMPLEMENT_DYNCREATE(CCgEditControl, CFormView)

CCgEditControl::CCgEditControl(): CFormView(IDD_EDITCONTROL)

{

}

CCgEditControl::~CCgEditControl()
{
}

void CCgEditControl::DoDataExchange(CDataExchange* pDX)
{
    CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCgEditControl, CFormView)
END_MESSAGE_MAP()
// CCgEditControl 诊断
#ifdef _DEBUG
void CCgEditControl::AssertValid() const
{
    CFormView::AssertValid();
}
#ifndef _WIN32_WCE
void CCgEditControl::Dump(CDumpContext& dc) const
{
    CFormView::Dump(dc);
}
#endif
#endif //_DEBUG
// CCgEditControl 消息处理程序


.h
//CCgEditControl.h
#pragma once
// CCgEditControl 窗体视图
class CCgEditControl : public CFormView
{
    DECLARE_DYNCREATE(CCgEditControl)
protected:
    CCgEditControl();           // 动态创建所使用的受保护的构造函数
    virtual ~CCgEditControl();
public:
#ifdef AFX_DESIGN_TIME
    enum { IDD =
IDD_EDITCONTROL };
#endif
#ifdef _DEBUG
    virtual void AssertValid() const;
#ifndef _WIN32_WCE
    virtual void Dump(CDumpContext& dc) const;
#endif
#endif
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

    DECLARE_MESSAGE_MAP()
};

第三步：
	修改idd资源号和类名字等

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
  {
    	// TODO: Add your specialized code here and/or call the base class
	if  (!m_wndSplit.CreateStatic(this, 1, 2))	
		return 0;	
	if  (!m_wndSplit.CreateView(0, 0,pContext->m_pNewViewClass,CSize(650, 100),pContext))   
		return 0;
	if  (!m_wndSplit.CreateView(0, 1,RUNTIME_CLASS(CCgEditControl),CSize(100,50),pContext))	
		return 0;
	ShowWindow(SW_SHOWMAXIMIZED); 
	return TRUE; 
  }
https://blog.csdn.net/Karen_Cassiopeia/article/details/102706239
https://blog.csdn.net/weixin_41303441/article/details/100749609?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-3.channel_param
)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}

jsoncpp_75S64D88M(){
	CommandText =
	(%
#include "json/json.h"
#define OK 0
int main()
{
	int rtn = OK;
    std::cout << "Hello World!\n"; 
	Json::Value root;
	root["name"] = "ahker";
	root["value"] = 100;

	std::cout << "as to" <<root["heliang"]["ahker"].asInt() << std::endl;
	std::cout << root["100"].type() << std::endl;

	if (root["name"].type() == Json::stringValue && rtn == OK)
	{
		std::cout << "name:" << root["name"].asString() << std::endl;
	}
	else { rtn = -1;}

	std::cout << root.toStyledString() << std::endl;
}
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

多线程_75S6EHLJT(){
	CommandText =
	(%
;mainScript
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
OnMessage(0x4a, "Receive_WM_COPYDATA")  ; 0x4a 为 WM_COPYDATA
gui,1:add, Button,section gthread ,runThread
gui,1:add,Edit,vThreadValue
gui,1:Show
return

thread()
{
    AhkDllPath := A_ScriptDir "\AutoHotkey.dll"
    hModule := DllCall("LoadLibrary","Str",AhkDllPath)
    DllCall(AhkDllPath "\ahkdll","Str","script1.ahk","Str","","Str","","Cdecl UPTR")
    return
}

Receive_WM_COPYDATA(wParam, lParam)
{
    global ThreadValue
    StringAddress := NumGet(lParam + 2*A_PtrSize)  ; 获取 CopyDataStruct 的 lpData 成员.
    CopyOfData := StrGet(StringAddress)  ; 从结构中复制字符串.
    ; 比起 MsgBox, 应该用 ToolTip 显示, 这样我们可以及时返回:
    ;ToolTip %A_ScriptName%`nReceived the following string:`n%CopyOfData%
    GuiControl, , ThreadValue , %CopyOfData%
    return true  ; 返回 1 (true) 是回复此消息的传统方式.
}

;threadScript
#NoEnv
#NoTrayIcon

TargetScriptTitle := "mutithread.ahk ahk_class AutoHotkey"
main()
return

main()
{
	global TargetScriptTitle
	StringToSend := "I'm a thread"
	result := Send_WM_COPYDATA(StringToSend, TargetScriptTitle)
	if (result = "FAIL")
		MsgBox SendMessage failed. Does the following WinTitle exist?:`n%TargetScriptTitle%
	else if (result = 0)
		MsgBox Message sent but the target window responded with 0, which may mean it ignored it.
	return
}

Send_WM_COPYDATA(ByRef StringToSend, ByRef TargetScriptTitle)  ; 在这种情况中使用 ByRef 能节约一些内存.
; 此函数发送指定的字符串到指定的窗口然后返回收到的回复.
; 如果目标窗口处理了消息则回复为 1, 而消息被忽略了则为 0.
{
    VarSetCapacity(CopyDataStruct, 3*A_PtrSize, 0)  ; 分配结构的内存区域.
    ; 首先设置结构的 cbData 成员为字符串的大小, 包括它的零终止符:
    SizeInBytes := (StrLen(StringToSend) + 1) * (A_IsUnicode ? 2 : 1)
    NumPut(SizeInBytes, CopyDataStruct, A_PtrSize)  ; 操作系统要求这个需要完成.
    NumPut(&StringToSend, CopyDataStruct, 2*A_PtrSize)  ; 设置 lpData 为到字符串自身的指针.
    Prev_DetectHiddenWindows := A_DetectHiddenWindows
    Prev_TitleMatchMode := A_TitleMatchMode
    DetectHiddenWindows On
    SetTitleMatchMode 2
    TimeOutTime := 4000  ; Optional. Milliseconds to wait for response from receiver.ahk. Default is 5000
    ; 必须使用发送 SendMessage 而不是投递 PostMessage.
    SendMessage, 0x4a, 0, &CopyDataStruct,, %TargetScriptTitle%  ; 0x4a 为 WM_COPYDAT
    DetectHiddenWindows %Prev_DetectHiddenWindows%  ; 恢复调用者原来的设置.
    SetTitleMatchMode %Prev_TitleMatchMode%         ; 同样.
    return ErrorLevel  ; 返回 SendMessage 的回复给我们的调用者.
}


	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

动态配置数组__75S7PEJEY(){
CommandText =
(%
	void make2dArray(T ** &x, int numberOfRaws, int *rowSize)
	{
		x = new T*[numberOfRaws];
		for (int i = 0; i < numberOfRaws; i++)
		{
			x[i] = new T[rowSize[i]];
		}
	}


	template<typename T>
	void delete2dArray(T ** &x, int numberOfRars)
	{
		for (int i = 0; i < numberOfRars; i++)
		{
			delete[] x[i];
		}
		delete[] x;
		x = NULL;
	}

)
Instruction := CommandText
clipboard := CommandText
Content := ""
TaskDialog(Instruction, Content, "", 0x1, 0)
}





json_v2_配置文件__75WMQ1WS7(){
	CommandText =
	(%

#include C:\Users\ahker\Desktop\我的AHK程序\库\v2\_JXON.ahk

json_path := A_ScriptDir . "/json.json"
config := Map()
loadconfig(&config)
config["Parent1"] := "ahker"
saveconfig(config)

loadconfig(&config)
{
    Global json_path
    OutputVar := FileRead(json_path)
    config := Jxon_load(&OutputVar)
}

saveconfig(config)
{
    global json_path
    str := Jxon_dump(config, 4)
    FileDelete(json_path)
    FileAppend(str, json_path)
}

	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}

lib_v1__75WO7LVUL(){
  	clipboard := "C:\Users\ahker\Desktop\我的AHK程序\库\v1\lib"
}
lib_v2__75WO7LVUW(){
  	clipboard := "C:\Users\ahker\Desktop\我的AHK程序\库\v2\lib"
}

log_75WOCE66S(){
	CommandText =
	(%
		spdlog(info, "war", A_LineFile, A_LineNumber)
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
tool_75WRO0MIM(){
	run,% A_ScriptDir "\cmd\menue_create.ahk"
}
edit_cmd_75WRO0N7B(){
	run,G:\vs code\Microsoft VS Code\Code.exe  %A_ScriptDir%\cmd\menucmd.ahk
}
计时器_time_75WROMGXS(){
	CommandText =
	(%
#include<chrono>
using namespace std;
using namespace std::chrono;
class Timer
{
public:
    Timer() : m_begin(high_resolution_clock::now()) {}
    void reset() { m_begin = high_resolution_clock::now(); }
    //默认输出毫秒
    int64_t elapsed() const
    {
        return duration_cast<chrono::milliseconds>(high_resolution_clock::now() - m_begin).count();
    }
    //微秒
    int64_t elapsed_micro() const
    {
        return duration_cast<chrono::microseconds>(high_resolution_clock::now() - m_begin).count();
    } 
    //纳秒
    int64_t elapsed_nano() const
    {
        return duration_cast<chrono::nanoseconds>(high_resolution_clock::now() - m_begin).count();
    }
    //秒
    int64_t elapsed_seconds() const
    {
        return duration_cast<chrono::seconds>(high_resolution_clock::now() - m_begin).count();
    }
    //分
    int64_t elapsed_minutes() const
    {
        return duration_cast<chrono::minutes>(high_resolution_clock::now() - m_begin).count();
    }
    //时
    int64_t elapsed_hours() const
    {
        return duration_cast<chrono::hours>(high_resolution_clock::now() - m_begin).count();
    }
private:
    time_point<high_resolution_clock> m_begin;
};
Timer t;
t.elapsed_micro()
	)
	Instruction := CommandText
	clipboard := CommandText
	Content := ""
	TaskDialog(Instruction, Content, "", 0x1, 0)
}
ReleaseDC_75WT5NP1N(){
}
GetDC_75WT5NP38(){
}
BitBit_75WT5NP3P(){
}
UpdateLayeredWindow_75WT5NP4E(){
}



编码转换_75WWDQHE3(){
run,http://www.mytju.com/classcode/tools/encode_utf8.asp
}




test_75WWGQHNN(){
}

